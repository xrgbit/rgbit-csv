(asdf:defsystem "rgbit-csv"
  :description "rgbit-csv: A simple library for reading and writing CSV (Comma Separated Value)."
  :license "GNU General Public License Version 2"
  :version "0.0.1"
  :author "Juan-Andres Martinez <juan.martinez.dev@protonmail.com>"
  :depends-on ("alexandria"
               "ltk"
               "dexador"
               "cl-json")
  :serial t
  :components ((:file "csv")))
