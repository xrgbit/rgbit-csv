(defpackage #:rgbit.csv
  (:use #:cl)
  (:local-nicknames (#:a #:alexandria))
  (:export #:*delimiter*
           #:parse-csv
           #:parse-csv-spec))

(in-package #:rgbit.csv)

(defun adjustable-string ()
  (make-array 0 :adjustable t :fill-pointer 0 :element-type 'character))

(defvar *delimiter* #\,)

(defun parse-csv-line (l)
  (loop :with cword := (adjustable-string)
        :with escapep
        :with stringp
        :for c :across l
        :if escapep
          :do (vector-push-extend c cword)
              (setf escapep nil)
        :else :if (char= c #\\)
                :do (setf escapep t)
        :else :if (char= c #\")
                :do (setf stringp (not stringp))
        :else :if stringp
                :do (vector-push-extend c cword)
        :else :if (char= c *delimiter*)
                :collect cword :into line
                :and :do
                  (setf cword (adjustable-string))
        :else :do
          (vector-push-extend c cword)
        :finally (return (append line (list cword)))))

(defun parse-csv (file-name)
  (with-open-file (f file-name)
    (loop :for line := (read-line f nil)
          :while line
          :collect (parse-csv-line line))))

(defun parse-csv-spec (file-name spec &key preprocessor postprocessor on-each)
  (funcall (or postprocessor #'identity)
           (loop :for l :in (funcall (or preprocessor #'identity)
                                     (parse-csv file-name))
                 :collect (funcall
                           (or on-each #'identity)
                           (loop :for (is processor) :in spec
                                 :append (loop :for i :in (a:ensure-list is)
                                               :collect (funcall (or processor #'identity) (nth i l))))))))

(defmacro mapping-csv ((column-spec file-name &key (readp t) ((:line-name !line) (gensym "LINE")))
                       &body collecting)
  (a:with-gensyms (!f !header)
    `(with-open-file (,!f ,file-name)
       (loop :with ,!header := (parse-csv-line (read-line ,!f nil))
             :for ,!line := (parse-csv-line (read-line ,!f nil))
             :for ,column-spec := (if ,readp
                                      (mapcar (lambda (x)
                                                (read-from-string x nil))
                                              ,!line)
                                      ,!line)
             :while ,!line
             :collect (progn ,@collecting)))))
