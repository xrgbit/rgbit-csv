# -*- mode:org -*-
#+title: rgbit-csv

* Overview
Just a really bare bones library for parsing CSV files.

Usage:
1. Load using ~(load "load.lisp")~ which will load the projects asd file
2. load using either ~QL:QUICKLOAD~ via ~(ql:quickload "rgbit-csv")~ or via
   ~ASDF:LOAD-SYSTEM~ via ~(adsf:load-system "rgbit-csv")~

The main functions are =PARSE-CSV= which takes a filename and reads the csv file delimited
by =RGBIT.CSV:*DELIMITER*=.

Features:
- Supports changing the delimiter via the special variable =RGBIT.CSV:*DELIMITER*=
- Escaping using =\=
- Double quoted strings which escape everything except for =\= and ="=
- Defining of parsing specs that can be used with =PARSE-CSV-SPEC=

** =PARSE-CSV-SPEC=
=PARSE-CSV-SPEC= Accepts a spec that defines a row in a csv file. The spec is a list of
column specs that correspond to the elements of the resulting list for each row.

- A column spec can simply be a number (which will be the value of that column, 0-indexed)
  (like =3= which will get the 4th column of the csv row or =NIL= if not preset)
- Or a list of two items
  - The first element is a list of numbers that correspond to columns
    - The first element can be a number which is equivalent to a singleton list containing
      that number
  - The second element is a function that takes 1 parameter and is applied to each of the
    elements before being collected. The element passed in the raw string parsed for the
    column.

For the sake of completeness a single number such as ~2~ is equivalent to the list ~((2)
IDENTITY)~


** Example Specs

The following spec parses a 3 elements of a csv row into a list of 4 elemens: column 0,
column 0, column 1, and column 2. The first two elements are parsed by =PARSE-MM/DD=, a
hypothetical function which returns a list of two items, from parsing a date string of the
format =MM/DD=. elements 3 and 4 are passed to =READ-FROM-STRING=.
#+begin_src lisp
  `((0 ,#'(lambda (x) (car (parse-mm/dd x))))
    (0 ,#'(lambda (x) (cadr (parse-mm/dd x))))
    ((1 2) ,#'read-from-string))
#+end_src
